#!/bin/sh
# postinst script for calendarserver
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <postinst> `abort-remove'
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package


case "$1" in
    configure)
        if ! getent passwd caldavd >/dev/null 2>&1; then
            adduser --system --home /var/spool/caldavd --no-create-home \
                    --gecos "calendarserver daemon" --group --disabled-password \
                    caldavd
        fi
        adduser caldavd ssl-cert

        for dir in /var/lib/caldavd /var/spool/caldavd /var/log/caldavd; do
            if [ -d $dir ]; then
                if ! dpkg-statoverride --list $dir >/dev/null 2>&1; then
                    chown caldavd: $dir
                fi
            fi
        done

        for x in calendaruserproxy.sqlite resourceinfo.sqlite mailgatewaytokens.sqlite tasks; do
            if [ -e /var/run/caldavd/$x ]; then
                echo -n "Moving /var/run/caldavd/$x to /var/lib/caldavd/$x (See Debian Bug#611165 for more info) ... "
                if [ ! -e /var/lib/caldavd/$x ]; then
                    mv /var/run/caldavd/$x /var/lib/caldavd/$x
                    echo "Done."
                else
                    echo "Aborted as /var/lib/caldavd/$x already exists."
                fi
            fi
        done

        for file in calendaruserproxy.sqlite resourceinfo.sqlite mailgatewaytokens.sqlite; do
            if [ -f /var/lib/caldavd/$file ]; then
                if ! dpkg-statoverride --list /var/lib/caldavd/$file >/dev/null 2>&1; then
                    chown caldavd: /var/lib/caldavd/$file
                    chmod 640 /var/lib/caldavd/$file
                fi
            fi
        done

        # Since calednarserver 7.x, we start calendarserver with "-u caldavd -g caldavd"
        # arguments and with this change, calendarserver fails to start unless the zoneinfo
        # folder is owned by caldavd user
        zoneinfo_dir="/var/lib/caldavd/zoneinfo"
        if [ -d $zoneinfo_dir ]; then
            if ! dpkg-statoverride --list $zoneinfo_dir >/dev/null 2>&1; then
                chown -R caldavd: $zoneinfo_dir
            fi
        fi
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
