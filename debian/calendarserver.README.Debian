Calendar and Contacts Server for Debian
=======================================

Upgrading
=========

Important notes before upgrading
--------------------------------

1. Before running upgrade script, ensure that /etc/caldavd/caladvd.plist
is configured properly with the correct directory service and database details.

2. If you are using XML directory service backend, you will have
to first upgrade the accounts.xml file to the latest format before you can
upgrade the database. See the "XML Directory Backend" section below.

3. If you are currently using files backend, then follow the instructions in
"Database Setup" section below to setup a database before you being the upgrade
processa as calendarserver no longer supports files backend for storing data.

Upgrade Process
---------------

If you have just upgraded calendarserver, you will have to run
calendarserver_upgrade to apply any necessary schema and data version upgrades
or else you might see errors as below in /var/log/caldavd/error.log.

2016-01-20 17:11:25+0100 [-] Log opened.
2016-01-20 17:11:25+0100 [-] [txdav.common.datastore.upgrade.sql.upgrade.UpgradeDatabaseSchemaStep#warn] Beginning database schema check.
2016-01-20 17:11:25+0100 [-] [txdav.common.datastore.upgrade.sql.upgrade.UpgradeDatabaseSchemaStep#warn] Required database key VERSION: 58.
2016-01-20 17:11:25+0100 [-] [txdav.common.datastore.upgrade.sql.upgrade.UpgradeDatabaseSchemaStep#warn] Actual database key VERSION: 28.
2016-01-20 17:11:25+0100 [-] [calendarserver.tap.caldav#error] Data store not available; shutting down
2016-01-20 17:11:25+0100 [-] [memcached-Default] Signal handled: Terminated.

The upgrade script is intended to be run immediately after upgrading to a
newer version, after making any necessary changes to config files and before
the caldavd service is started.

 # chown caldavd: /etc/caldavd/resources.xml /etc/caldavd/augments.xml
 # sudo -u caldavd calendarserver_upgrade

To get a list of all options:

 # sudo -u caldavd calendarserver_upgrade --help


Database Setup
==============

Calendar and Contacts Server can currently use postgresql for storing users'
calendars and addressbooks. Files backend used to be supported in earlier
versions and is now obsoleted.

Bootstrapping (for new database setup)
--------------------------------------

1. Install postgresql server.

 # sudo apt-get install postgresql

2. Next, you will have to create a postgresql user. If postgresql and
calendarserver are running on the same machine, it is preferable to use peer
authentication rather than password authentcation.

For peer authentication, run:
 # sudo -u postgres createuser caldavd --no-createdb --no-createrole --no-superuser

For password authentication, run:
 # sudo -u postgres createuser caldavd --no-createdb --no-createrole --no-superuser --pwprompt
 Enter password for new role: <Enter password>
 Enter it again: <Reenter password>

3. Now create the database.

 # sudo -u postgres createdb --owner=caldavd caldav

4. Finally, import the calendarserver schema

 # sudo -u caldavd psql -f /usr/lib/python2.7/dist-packages/txdav/common/datastore/sql_schema/current.sql caldav


Configuration
-------------

For peer authentication, configure postgres details in caldavd.plist
as below:

    <key>DBType</key>
    <string>postgres</string>
    <key>DatabaseConnection</key>
    <dict>
      <key>endpoint</key>
      <string>unix:/var/run/postgresql</string>
      <key>database</key>
      <string>caldav</string>
      <key>user</key>
      <string>caldavd</string>
      <key>password</key>
      <string></string>
    </dict>


For password authentication, configure postgres details in caldavd.plist
as below:

    <key>DBType</key>
    <string>postgres</string>
    <key>DatabaseConnection</key>
    <dict>
        <key>endpoint</key>
        <string>tcp:localhost</string>
        <key>database</key>
        <string>caldav</string>
        <key>user</key>
        <string>caldavd</string>
        <key>password</key>
        <string>caldavd_user_password</string>
    </dict>

where caldavd_user_password is the password that you have set for postgresql
caldavd user above.


Migration from files to DB
--------------------------

Migration of data from files to DB is automatically taken care by
calendarserver, once the DB is setup properly and calendarserver is started.
When the server starts, if there is any calendar data in the DocumentRoot,
that data will be upgraded and imported into the database. This is configured
to happen automatically.

However, sometimes users and groups information is cached in memcached and does
not get updated al though migration happens properly. Restarting memcached should
fix this.


Basic Setup (Using XML Directory Service Backend)
=================================================

You have to add a /etc/caldavd/accounts.xml to tell caldavd about your accounts
and users. See /usr/share/doc/calendarserver/examples/accounts.xml for an
example.

The URI to your CalDAV calendar will typically look like:

 http://<server>:8008/calendars/users/<user>/calendar/

where <server> is the hostname or the IP of the server on which calendarserver
is installed and <user> is the username of a calendarserver user as specified 
in accounts.xml. Similarly, the URI to your CardDAV addressbook will look like:

 http://<server>:8008/addressbooks/users/<user>/addressbook/


Loadbalancing
=============

In order to enable load balancing onto different processors/cores you need to
set the ProcessCount in /etc/caldavd/caldavd.plist accordingly.


Enabling SPNEGO/Kerberos
========================

To make SPNEGO/Kerberos authentication work you have to add service principals
for HTTP/caldavd.example.com@EXAMPLE.COM and
http/caldavd.example.com@EXAMPLE.COM to your servers keytab /etc/krb5.keytab
(replace caldavd.example.com by the fqdn of your caldav server and EXAMPLE.COM
by your Kerberos realm).

The uppercase http variant is used by most clients like iceowl and icedove with
iceowl-extension while the lowercase version is used by iCal. The keytab must
be readable for user caldavd which can be achieved by:

chgrp caldavd /etc/krb5.keytab
chmod 0640 /etc/krb5.keytab

Now you have to specify the name of a ServicePrincipal in
/etc/caldavd/caldavd.plist:

    <!-- Kerberos/SPNEGO -->
    <key>Kerberos</key>
    <dict>
      <key>Enabled</key>
      <true/>
      <key>ServicePrincipal</key>
      <string>http/caldavd.example.com@EXAMPLE.COM</string>
    </dict>

Note: if you use iceowl/iceowl-extension the
network.negotiate-auth.trusted-uris in iceowl/icedove must match on your
calendarservers uri otherwise SPNEGO will not work. A good choice is
"https://".


XML Directory Backend
=====================

1. In calendarserver versions earlier to 5.x, the guid for XML directory records
were automatically generated by calendarserver code. Starting with version 5.x,
guid for xml directory records are no longer automatically generated and have
to be explicitly added.

If you are upgrading from earlier versions and have been using XML directory
records with auto-generated GUIDs, then it is important that the new GUIDs that
you will be manually adding will match the earlier auto-generated GUIDs. To get
the value of the earlier auto-generated GUID, run the below script:

$ python /usr/share/doc/calendarserver/scripts/generate-guid-for-xml-directory-record.py \
--realm <realm> --record-type <users|groups|locations|resources> --uid <uid>

For ex.

$ python /usr/share/doc/calendarserver/scripts/generate-guid-for-xml-directory-record.py \
--realm "Test Realm" --record-type users --uid test

2. From 5.x to 7.x, there has been significant change in the structure of the
accounts.xml file. The example config files in /usr/share/doc/calendarserver/examples/
for calendarserver version 3.x or 5.x could be compared with that of 7.x to
understand the differences and implement them. Here is an example which gives a
summary of the changes (thanks to Ximin Luo for pointing out these changes):
~~~~ old ~~~~
<accounts realm="Test Realm">
  <user>
    <uid>admin</uid>
    <guid>38EAE584-2B75-582B-A058-018391D1003F</guid>
    <password>ZZZZ</password>
    <name>Super User</name>
  </user>
</accounts>
~~~~ new ~~~~
<directory realm="Test Realm">
  <record>
    <uid>38EAE584-2B75-582B-A058-018391D1003F</uid> <!-- i.e. same as guid -->
    <guid>38EAE584-2B75-582B-A058-018391D1003F</guid>
    <password>ZZZZ</password>
    <short-name>admin</short-name> <!-- i.e. same as previous uid -->
    <full-name>Super User</full-name> <!-- i.e. same as previous name -->
  </record>
</directory>
~~~~

Once accounts.xml file is modified, calendserver_upgrade could be run (see
above) to migrate resources and augments xml files.


Nameservice Switch Backend
==========================

If you don't want to manage accounts in a separate XML file you can use the
names service switch backend. Details on how to set this up can be found at:

http://honk.sigxcpu.org/con/Apple_Calendarser_with_Name_Service_Switch_directory_backend.html

Note that in order to function properly "getent passwd" must list all users
that should be able to access the calendarserver. This might not be the case if
you hit the search limit of your LDAP server.

Also note that the username and groupname (without the prefix) shouldn't be the
same else calendarserver might get confused.

PAM authentication is supported (python-pam needs to be installed). The pam 
service name is "caldav". Basic Authentication should be enabled and Digest 
Authentication should be disabled for PAM authentication to work. It is 
suggested to use PAM authentication in conjunction with SSL (https) so that the
password is not sent in plain text.

    <!-- Clear text; best avoided -->
    <key>Basic</key>
    <dict>
      <key>Enabled</key>
      <true/>
      <!-- Set to false to disallow plaintext authentication over non-SSL -->
      <key>AllowedOverWireUnencrypted</key>
      <true/>
    </dict>

    <!-- Digest challenge/response -->
    <key>Digest</key>
    <dict>
      <key>Enabled</key>
      <false/>
      <key>Algorithm</key>
      <string>md5</string>
      <key>Qop</key>
      <string></string>
    </dict>

Aso, for PAM authentication against local unix passwords, the user "caldavd"
should be added to the "shadow" group.

$ sudo adduser caldavd shadow

 -- Guido Guenther <agx@sigxcpu.org>  Thu, 30 Apr 2008 16:17:56 +0100
 -- Rahul Amaram <amaramrahul@users.sourceforge.net>  Mon, 18 Jan 2016 12:27:53 +0530


LDAP Directory Backend
======================

LDAP Directory service has been merged upstream in calendarserver 3.0
(python-ldap package has to be installed for using this directory service).
Information on configurating LDAP can be found at: 

    http://trac.calendarserver.org/wiki/ConfiguringLDAP

 -- Rahul Amaram <amaramrahul@users.sourceforge.net>  Tue, 24 Aug 2010 22:05:00 +0530


FAQ
===

Calendar and Contacts Server FAQ is maintained at:

    http://trac.calendarserver.org/wiki/FAQ

